"use strict";

module.exports = RootedTree;

var ColoredTree = require("ColoredTree");
var l = require("loglevel");

function RootedTree(seed, forest) {
  this.seed = seed;
  this.forest = forest;
}

RootedTree.prototype = {
  get size() {
    return this.forest.size + 1;
  },
  eq: function(tree) {
    if(tree === undefined || this.size != tree.size) {
      return false;
    }
    return this.seed.eq(tree.seed);
  },
  compare: function(tree) { 
    if(this.size > tree.size) {
      return 1;
    } else if(this.size < tree.size) {
      return -1;
    } else {
      return -(this.seed.compare(tree.seed) | 0);
    }
  },
  copy: function() {
    return new RootedTree(this.seed, this.forest);
  },
  deepCopy: function() {
    return new RootedTree(this.seed, this.forest.deepCopy());
  },
  asColoredTree: function(num) {
    if(num === undefined) {
      num = 0;
    }
    return new ColoredTree(num, this.forest.asColoredTrees(num + 1));
  },
  childrenAccessor: function(d) {
    return d.forest.trees;
  }
};

