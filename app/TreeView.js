"use strict";

module.exports = TreeView;

var d3 = require("d3");
var l = require("loglevel");

function TreeView(tree, config) {

  this.tree = tree.deepCopy();
  if(!config.width || !config.width) {
    config.width = 400;
    config.height = 220;
  }
  if(!config.radius) {
    config.radius = 5;
  }
  this.config = config;
  this.compute();
}

TreeView.prototype = {
  compute: function() {
    var d3tree = d3.layout.tree().size([
      this.config.width - 4*this.config.radius,
      this.config.height - 4*this.config.radius]);
    d3tree.children(Object.getPrototypeOf(this.tree).childrenAccessor);
    this.nodes = d3tree.nodes(this.tree);
    this.links = d3tree.links(this.nodes);
    this.nodes[0].root = true;
  },
  setOnclickNode: function(callback) {
    this.nodeListener = callback;
  },
  render: function() {
    
    var config = this.config;
    // Create SVG element
    var svg = config.div.append("svg")
    .attr("width", config.width)
    .attr("height", config.height)
    .append("g");

    // Diagonal to draw links
    var diagonal = d3.svg.diagonal();

    // node and link svg elements
    svg.selectAll(".link")
    .data(this.links)
    .enter().append("path")
    .attr("class", "link")
    .attr("d", diagonal)
    .attr("transform", "translate(0," + 2*config.radius + ")");

    var node = svg.selectAll(".node")
    .data(this.nodes)
    .enter().append("g")
    .attr("class", "node")
    .attr("transform", function(d) {
      return "translate(" + d.x + "," + (d.y + 2*config.radius) + ")";
    });

    if(this.nodeListener) {
      node.on("click", this.nodeListener);
    }
    
    var circles = node.append("circle").attr("r", config.radius);
    if(config.displayColors) {
      circles.style("fill", function(d) {
        if(d.color) {
          return d.color.isWhite ? "#ffffff" : "#000000";
        } else {
          return d.root ? "#99bbff" : "#ffffff";
        }
      });
    } else {
      circles.style("fill", d => d.root ? "#99bbff" : "#ffffff");
    }

    if(config.displayNums) {
      node.append("text") .text( function(d) {
        return d.num;
      }).attr("dx", 9).attr("dy", -2);
    }
  }
};
