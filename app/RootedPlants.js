"use strict";

var BigInt = require("big-integer");
var Tree = require("RootedTree");
var Forest = require("RootedForest");

exports.tree = tree;
exports.treesOfSize = treesOfSize;
exports.forest = forest;
exports.startGrower = startGrower;

var grower;

function startGrower(maxSize) {
  grower = new Grower(maxSize);
}

function tree(size, seed) {
  if(seed === undefined) {
    seed = grower.randomSeed(size);
  }
  return grower.grow(size, new BigInt(seed));
}

function treesOfSize(size) {
  var n = grower.maxSeed(size).toJSNumber();
  var trees = new Array(n + 1);
  for(var i = 0; i <= n; i++) {
    trees[i] = grower.grow(size, new BigInt(i));
  }
  return trees;
}

function forest(size, seed) {
  var t = tree(size + 1, seed);
  t.forest.seed = t.seed;
  return t.forest;
}


function Grower(m) {

  var runtable, remtable, lentable, partable, comtable;
  fillTables();

  // The size of the biggest tree you can generate
  this.maxSize = () => m; 

  // The biggest index of a tree's root for a given size
  this.maxSeed = function(size) {
    return comtable[size-1].minus(1); 
  };

  this.randomSeed = function(size) {
    return BigInt.randBetween(0, comtable[size-1].minus(1));
  };

  this.grow = function(size, seed) {
    return growTree(size, seed, 0);
  };

  function growTree(size, seed) {
    return new Tree(seed, growForest(size - 1, seed, size - 1));
  }
  
  function growForest(size, seed, maxhead) {
    if(maxhead > size) {
      maxhead = size;
    }
    if(maxhead <= 0) {
      return new Forest(new Array(0), 0);
    }
    var reseed = seed;
    var head = maxhead;
    var par;
    while((par = partable[size-1][head-1]).leq(reseed)) {
      reseed = reseed.minus(par);
      head = head - 1;
    }
    // This is the maximum value because
    // in lentable_ijk you have k <= i / j;
    var frontlength = (size / head) | 0;
    // We do exactly the same thing as `ptcoords` but with lentable.
    var len;
    while ((len = lentable[size-1][head-1][frontlength-1]).leq(reseed)) {
      reseed = reseed.minus(len);
      frontlength = frontlength - 1;
    }
    // This should be :
    // var c = len.divide(runtable[head-1][frontlength-1]);
    // But because it's actually the rem of L_ijk = rem*R_jk,
    // We memoize the thing in a `remtable`.
    var rem = remtable[size-1][head-1][frontlength-1];
    var frontseed = reseed.divide(rem);
    var remainderseed = reseed.mod(rem);

    var front = growFront(head, frontseed, frontlength);
    var remainder = growForest(
      size - front.size,
      remainderseed,
      head - 1);
    return front.merge(remainder);
  }

  function growFront(head, seed, length) {
    var trees = new Array(length);
    var t, end, beg, mid, sup, prev, bin1, bin2, total, pen;
    var found, isLess, isMore;
    prev = new BigInt(0);
    sup = comtable[head-1];
    for(t = length, total = runtable[head-1][length-1]; t > 1; t--, total = bin(sup,t)){
      pen = total.minus(1).minus(seed);
      end = sup.plus(1);
      beg = new BigInt(1);
      mid = end.plus(beg).divide(2);
      found = false;
      while(!found) {
        if(mid.isUnit()) {
          bin2 = new BigInt(0);
          bin1 = new BigInt(1);
        } else {
          bin2 = bin(mid.minus(1),t);
          bin1 = bin2.plus(bin2.times(t).divide(mid.minus(1)));
        }
        isLess = bin1.gt(pen);
        isMore = pen.geq(bin2);
        found = isLess && isMore;
        if(!found) {
          if(isLess) {
            end = mid;
          } else {
            beg = mid;
          }
          mid = end.plus(beg).divide(2);
        }
      }
      prev = sup.minus(mid).plus(prev);
      trees[length - t] = growTree(head, prev);
      sup = mid;
      seed = seed.minus(total).plus(bin1);
    }
    trees[length - 1] = growTree(head, seed.plus(prev));
    return new Forest(trees, length * head);
  }

  // Computes (x + t - 1) `choose` t
  function bin(x, t) {
    var val;
    if(t == 1 || x.isUnit()) {
      val = x;
    } else {
      var prec = bin(x,t-1);
      val = prec.plus(prec.times(x.minus(1)).divide(t));
    }
    return val;
  }

  // Setup for the algorithm ; called at instanciation
  function fillTables() {
    var run, rem, len, par, com; // last values of the tables
    var i, j, k, v; // loops index
    var kmax; // for the weird condition on k
    var u; // i-(j+1)*(k+1) ; useful for lentable[i][j][k]
    runtable = new Array(m-1);
    remtable = new Array(m-1);
    lentable = new Array(m-1);
    partable = new Array(m-1);
    comtable = new Array(m);
    com = new BigInt(1); 
    comtable[0] = com;
    for(i = 1; i < m; i++){
      runtable[i-1] = new Array((m-1) / i | 0);
      remtable[i-1] = new Array(i);
      lentable[i-1] = new Array(i);
      partable[i-1] = new Array(i);
      // Fill the current vector in the runtable 
      run = new BigInt(1);
      kmax = ((m-1) / i) | 0;
      for(k = 1; k <= kmax ; k++){
        run = run.times(com.plus(k-1)).divide(k);
        runtable[i-1][k-1] = run;
      }
      // Fill the remtable, causing the filling of the other tables
      com = new BigInt(0);
      for(j = 1; j <= i; j++){
        remtable[i-1][j-1] = new Array((i / j) | 0);
        lentable[i-1][j-1] = new Array((i / j) | 0);
        par = new BigInt(0);
        kmax = (i / j) | 0;
        for(k = 1; k <= kmax | 0; k++){
          u = i-j*k;
          if(u === 0){
            rem = new BigInt(1);
          } else {
            rem = new BigInt(0);
            for (v = 1; v <= Math.min(j-1,u); v++){
              rem = rem.plus(partable[u-1][v-1]);
            }
          }
          remtable[i-1][j-1][k-1] = rem;
          // The lentable is a product of an element of remtable
          // and an element of runtable
          len = runtable[j-1][k-1].times(rem);
          lentable[i-1][j-1][k-1] = len;
          // The partable is the sum of the towers of the lentable
          par = par.plus(len);
        }
        partable[i-1][j-1] = par;
        // The comtable is the sum of the columns of the partable
        com = com.plus(par);
      }
      comtable[i] = com;
    }
  }
}
