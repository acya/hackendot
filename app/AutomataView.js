"use strict";

var RootedForestView = require("RootedForestView");
// var l = require("loglevel");

module.exports = AutomataView;

function AutomataView(automata, config) {
  var states = automata.states;
  this.forestViews = [];

  var self = this;
  
  var st, stdiv;
  var s, sdiv, c, fw;
  for(let i = states.length - 1; i >= 0; i --) {
    st = states[i];
    stdiv = config.div.append("div");
    stdiv.attr("class", "stcontainer");
    for(let j = 0; j < st.length; j++) {
      s = st[j];
      s.name = "state" + s.forest.size + "-" + j;
      sdiv = stdiv.append("div");
      sdiv.attr("class", "scontainer");
      sdiv.attr("id", s.name);
      c = Object.assign({}, config);
      c.div = sdiv;
      fw = new RootedForestView(s.forest, c);
      fw.setOnclickNode(function(view, transitions) {
        return function(d) {
          view.goTo(transitions[d.num]);
        };
      }(self, s.transitions));
      this.forestViews.push(fw);
    }
  }

  this.render = function() {
    this.forestViews.forEach(function(fw) {
      fw.render();
    });
  };

  this.goTo = function(s) { 
    var loc = document.location.toString().split("#")[0];
    document.location = loc + "#" + s.name;
    highlight(document.getElementById(s.name));
  };
}

function highlight(elt) {
  var tic = 60; 
  var opacity = 0.7;
  elt.style.backgroundColor = "rgba(255, 0, 0," + opacity + ")";
  var fading = setInterval(function() {
    opacity = opacity*0.7 - 0.002;
    if(opacity <= 0) {
      elt.style.backgroundColor = "rgba(255, 0, 0, 0)";
      clearInterval(fading);
    }
    elt.style.backgroundColor = "rgba(255, 0, 0," + opacity + ")";
  }, tic);
}
