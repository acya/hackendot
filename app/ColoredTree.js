"use strict";

module.exports = ColoredTree;

var l = require("loglevel");

function Color(isWhite) {
  this.isWhite = isWhite;
}

Color.prototype = {
  invert: function() {
    return Color.get(!this.isWhite);
  }
};

Color.white = new Color(true);
Color.black = new Color(false);

Color.get = function(isWhite) {
  if (isWhite) {
    return Color.white;
  } else {
    return Color.black;
  }
};

function ColoredTree(num, children) {
  this.num = num;
  if (children === undefined) {
    this.children = [];
    this.color = Color.get(true);
  } else {
    this.children = children;
    let hasWhiteChild = false;
    let i = 0;
    while(!hasWhiteChild && i < this.length) {
      hasWhiteChild = this.children[i++].color.isWhite;
    }
    this.color = Color.get(!hasWhiteChild);
  }
}

ColoredTree.prototype = {
  get length() {
    return this.children.length;
  },
  copy: function() {
    return new ColoredTree(this.num, this.children);
  },
  deepCopy: function() {
    var c = this.children.map(t => t.deepCopy());
    return new ColoredTree(this.num, c);
  },
  isLeaf: function() {
    return this.length == 0;
  },
  recolor: function() {
    if (this.isLeaf()) {
      this.color = Color.get(true);
    } else {
      this.children.forEach(function(t) {
        t.recolor();
      });
      let hasWhiteChild = false;
      let i = 0;
      while (!hasWhiteChild && i < this.length) {
        hasWhiteChild = this.children[i++].color.isWhite;
      }
      this.color = Color.get(!hasWhiteChild);
    }
  },
  deleteWhiteNodes: function() {
    var children = ColoredTree.deleteWhiteNodes(this.children);
    if(this.color.isWhite) {
      return children;
    } else {
      return [new ColoredTree(this.num, children)];
    }
  },
  deleteNode: function(n) {
    if(n == this.num) {
      return this.children;
    }
    if(this.children.length == 0) {
      return [this];
    }
    var t, d;
    for(var i = 0; i < this.children.length; i++) {
      t = this.children[i];
      d = t.deleteNode(n);
      if(d.length != 1 || d[0] !== t) {
        break;
      }
    }
    if(i == this.children.length) {
      return [this];
    }
    var c = this.children.slice(0);
    c.splice(i, 1);
    return c.concat(d);
  },
  getNode: function(n) {
    if(n == this.num) {
      return this;
    }
    if(this.children.length == 0) {
      return null;
    }
    return ColoredTree.getNode(this.children, n);
  },
  childrenAccessor: function(d) {
    return d.children;
  }
};

ColoredTree.deleteNode = function(trees, n) {
  return trees.map(t => t.deleteNode(n)).reduce((f1, f2) => f1.concat(f2), []);
};

ColoredTree.getNode = function(trees, n) {
  var t;
  var i = 0;
  while(i < trees.length && ((t = trees[i].getNode(n)) == null)) {
    i++;
  }
  if(i == trees.length) {
    return null;
  }
  return t;
};

ColoredTree.deleteWhiteNodes = function(trees) {
  return trees.map(t => t.deleteWhiteNodes())
    .reduce((f1, f2) => f1.concat(f2), []);
};

ColoredTree.countWhiteRoots = function(trees) {
  return trees.map(t => t.color.isWhite | 0).reduce((a, b) => a + b, 0);
};

