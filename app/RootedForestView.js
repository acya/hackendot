"use strict";

module.exports = RootedForestView;

var RootedTree = require("RootedTree");
var TreeView = require("TreeView");
var d3 = require("d3");
var l = require("loglevel");

function computeNums(tree, num) {
  tree.num = num++;
  var child;
  for(let i = 0; i < tree.forest.length; i++) {
    child = tree.forest.trees[i];
    computeNums(child, num);
    num += child.size;
  }
}

function RootedForestView(forest, config) {
  TreeView.call(this, new RootedTree(undefined, forest), config);
}

RootedForestView.prototype = Object.create(TreeView.prototype, {
  compute: {
    value: function() {
      computeNums(this.tree, -1);
      var d3tree = d3.layout.tree().size([
        this.config.width - 4*this.config.radius,
        this.config.height - 4*this.config.radius]);
      d3tree.children(RootedTree.prototype.childrenAccessor);
      this.nodes = d3tree.nodes(this.tree);
      this.nodes[0].forest.trees.forEach(function(n) {
        n.root = true;
      });
      var offset = 0;
      if(this.nodes[0].forest.length > 0) {
        offset = -this.nodes[0].forest.trees[0].y;
      }
      this.config.height += offset;
      this.nodes.shift();
      this.nodes.forEach(function(n) {
        n.y += offset;
      });
      this.links = d3tree.links(this.nodes);
    },
    enumerable: true,
    configurable: true,
    writable: true
  }
});
