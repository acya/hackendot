"use strict";

module.exports = RootedForest;

// var l = require("loglevel");

function RootedForest(trees, size) {
  if(size === undefined) {
    size = 0;
    for(let i = 0; i < trees.length; i++) {
      size += trees[i].size;
    }
  }
  if(trees.length == 0) {
    this.trees = new Array(size);
  } else {
    this.trees = trees;
  }
  this.size = size;
}

RootedForest.prototype = {
  get length() {
    return this.trees.length;
  },
  empty: function() {
    return this.size == 0;
  }, 
  copy: function() {
    return new RootedForest(this.trees.slice(0), this.size);
  },
  deepCopy: function() {
    return new RootedForest(this.trees.map(t => t.deepCopy()), this.size);
  },
  eq: function(forest) {
    if(forest === undefined ||
       this.length != forest.length ||
         this.size != forest.size) {
      return false;
    }
    var same = true;
    for(let i = 0; i < this.length && same; i++) {
      same = this.trees[i].eq(forest.trees[i]);
    }
    return same;
  },
  first: function() {
    return this.trees[0];
  }, 
  last: function() {
    return this.trees[this.length - 1];
  },
  shift: function() {
    this.size -= this.first().size;
    return this.trees.shift();
  },
  pop: function() {
    this.size -= this.last().size;
    return this.trees.pop();
  },
  insert: function(tree) {
    this.size += tree.size;
    for(var i = this.length - 1; i <= 0; i--) {
      if(this.trees[i].compare(tree) >= 0) {
        break;
      }
    }
    this.trees.splice(i, 0, tree);
  },
  forEach: function(callback) {
    for(let i = 0; i < this.length; i++) {
      callback(this.trees[i], i, this);
    }
  },
  merge: function(forest) {
    var size, trees, copyThis;
    size = this.size + forest.size;
    trees = new Array(this.length + forest.length);
    copyThis = forest.empty();

    var putThisFirst;
    if(copyThis || this.empty()) {
      var toCopy = copyThis ? this : forest;
      trees = toCopy.trees.slice(0);
    } else if(
      (putThisFirst = this.last().compare(forest.first()) >= 0)
        || forest.last().compare(this.first()) >= 0) {
      var toPutFirst = putThisFirst ? this : forest;
      var toPutLast = putThisFirst ? forest : this;
      trees = toPutFirst.trees.concat(toPutLast.trees);
    } else {
      var i = 0, j = 0;
      var treeThis;
      var treeForest;
      while(i < this.length && j < forest.length) {
        treeThis = this.trees[i];
        treeForest = forest.trees[j];
        if(treeThis.compare(treeForest) >= 0) {
          trees[i + j] = treeThis;
          i++;
        } else {
          trees[i + j] = treeForest;
          j++;
        }
      }
      for(let u = i; u < this.length; u++) {
        trees[u + forest.length] = this.trees[u];
      }
      for(let u = j; u < forest.length; u++) {
        trees[this.length + u] = forest.trees[u];
      }
    }
    return new RootedForest(trees, size);
  },
  deleteRootAndTrace: function(i) {
    var rest = this.trees.slice(0);
    var tree = rest.splice(i, 1)[0];
    return (new RootedForest(rest, this.size - tree.size))
      .mergeAndTrace(tree.forest);
  },
  deleteRoot: function(i) {
    var rest = this.trees.slice(0);
    var tree = rest.splice(i, 1)[0];
    return (new RootedForest(rest, this.size - tree.size))
      .merge(tree.forest);
  },
  deleteNode: function(n) {
    var i = 0, num = 0;
    if(this.empty()) {
      throw "EmptyForest";
    }
    if(n == 0) {
      return this.deleteRoot(0);
    }
    while((num += this.trees[i].size) <= n && i < this.length) {
      i++;
    }
    if(i >= this.length) {
      throw "UnboundException";
    }
    var tree = this.trees[i];
    num -= tree.size;
    if(num == n) {
      return this.deleteRoot(i);
    }
    var merger = this.deleteRootAndTrace(i);
    i = 0;
    while((num += tree.forest.trees[i].size) < n) {
      i++;
    }
    num -= tree.forest.trees[i].size;
    return merger.forest.deleteNode(merger.trace[i] + n - num - 1);
  },
  mergeAndTrace: function(forest) {
    var size = this.size + forest.size;
    var trees = new Array(this.length + forest.length);
    var trace = new Array(forest.length);
    var antitrace = new Array(this.length);
    var num = 0;

    if(forest.empty()) {
      trees = this.trees.slice(0);
      for(let i = 0; i < this.length; i++) {
        antitrace[i] = num;
        num += this.trees[i].size;
      }
    } else if(this.empty()) {
      num = 0;
      for(let i = 0; i < forest.length; i++) {
        trace[i] = num;
        num += forest.trees[i].size;
      }
      trees = forest.trees.slice(0);
    } else if(this.last().compare(forest.first()) >= 0) {
      for(let i = 0; i < this.length; i++) {
        antitrace[i] = num;
        num += this.trees[i].size;
      }
      for(let i = 0; i < forest.length; i++) {
        trace[i] = num;
        num += forest.trees[i].size;
      }
      trees = this.trees.concat(forest.trees);
    } else if(forest.last().compare(this.first()) >= 0) {
      for(let i = 0; i < forest.length; i++) {
        trace[i] = num;
        num += forest.trees[i].size;
      }
      for(let i = 0; i < this.length; i++) {
        antitrace[i] = num;
        num += this.trees[i].size;
      }
      trees = forest.trees.concat(this.trees);
    } else {
      var i = 0, j = 0;
      var treeThis;
      var treeForest;
      while(i < this.length && j < forest.length) {
        treeThis = this.trees[i];
        treeForest = forest.trees[j];
        if(treeThis.compare(treeForest) >= 0) {
          trees[i + j] = treeThis;
          antitrace[i] = num;
          num += treeThis.size;
          i++;
        } else {
          trees[i + j] = treeForest;
          trace[j] = num;
          num += treeForest.size;
          j++;
        }
      }
      for(let u = i; u < this.length; u++) {
        treeThis = this.trees[u];
        trees[u + forest.length] = this.trees[u];
        antitrace[u] = num;
        num += treeThis.size;
      }
      for(let u = j; u < forest.length; u++) {
        treeForest = forest.trees[u];
        trees[this.length + u] = treeForest;
        trace[u] = num;
        num += treeForest.size;
      }
    }
    return {
      forest: new RootedForest(trees, size),
      trace: trace,
      antitrace: antitrace
    };
  },
  asColoredTrees: function(num) {
    if(num === undefined) {
      num = 0;
    }
    var trees = [];
    var tree;
    for (var i = 0; i < this.length; i++) {
      tree = this.trees[i];
      trees.push(tree.asColoredTree(num));
      num += tree.size;
    }
    return trees;
  },
};
 

