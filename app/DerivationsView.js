"use strict";

var ColoredForestView = require("ColoredForestView");
var l = require("loglevel");

module.exports = DerivationsView;

function DerivationsView(ulehla, config) {
  
  var L = ulehla.L;
  this.forestViews = [];
  
  var l, ldiv, c, fw;
  for(let i = 0; i < L.length; i++) {
    l = L[i];
    ldiv = config.div.append("div");
    ldiv.attr("class", "lcontainer");
    c = Object.assign({}, config);
    c.div = ldiv;
    fw = new ColoredForestView(l, c);
    this.forestViews.push(fw);
  }

  this.render = function() {
    this.forestViews.forEach(function(fw) {
      fw.render();
    });
  };
}
