"use strict";

module.exports = KernelAI;

// var l = require("loglevel");

function KernelAI(forest) {
  var automata = new Automata(forest.size);
  this.automata = automata;
  initstates();
  resolvetransitions();
  this.current = automata.states[forest.size][0];

  function initstates() {
    var maxSize = forest.size;
    automata.add([new State(forest, null, 0, null)]);
    var st;
    for(let i = maxSize; i > 0; i--) {
      st = automata.states[i];
      for(let j = 0; j < st.length; j++) {
        initfollowing(st[j]);
      }
    }
  }

  function resolvetransitions() {
    var st, s, t;
    for(let i = 1; i <= forest.size; i++) {
      st = automata.states[i];
      for(let j = 0; j < st.length; j++) {
        s = st[j];
        for(let k = 0; k < s.transitions.length; k++) {
          t = s.transitions[k].resolve();
          s.transitions[k] = t;
          s.kernel = s.kernel && !t.kernel;
        }
      }
    }
  }

  function initfollowing(state) {
    var merger, tree, child;
    var children = [];
    var num = state.forest.size;
    var nums = new Array(state.forest.length);
    var duplicate = new Array(state.forest.length);
    for(let i = state.forest.length - 1; i >= state.minindex; i--) {
      if(!state.forest.trees[i].eq(tree)) {
        duplicate[i] = false;
        tree = state.forest.trees[i];
        merger = state.forest.deleteRootAndTrace(i);
        child = new State(merger.forest, state, i);
        children.unshift(child);
      } else {
        duplicate[i] = true;
        child.minindex--;
      }
      state.traces[i] = merger.antitrace;
      for(let j = tree.forest.length - 1; j >= 0; j--) {
        for(let k = tree.forest.trees[j].size - 1; k >= 0; k--) {
          state.transitions[--num] = new Transition(child, merger.trace[j] + k);
        }
      }
      state.transitions[--num] = new Transition(child);
      nums[i] = num;
    }
    tree = undefined;
    for(let i = state.minindex - 1; i >= 0; i--) {
      if(!state.forest.trees[i].eq(tree)) {
        duplicate[i] = false;
        tree = state.forest.trees[i];
        merger = state.forest.deleteRootAndTrace(i);
      } else {
        duplicate[i] = true;
      }
      state.traces[i] = merger.antitrace;
      let n = num;
      num -= tree.size;
      child = state.parent.transitions[num].state
        .transitions[state.parent.traces[i][state.minindex - 1]].state;
      for(let j = tree.forest.length - 1; j >= 0; j--) {
        for(let k = tree.forest.trees[j].size - 1; k >= 0; k--) {
          state.transitions[--n] = new Transition(child, merger.trace[j] + k);
        }
      }
      state.transitions[num] = new Transition(child);
      nums[i] = num;
    }
    automata.add(children);
  }
}

KernelAI.prototype = {
  move: function(n) {
    this.current = this.current.transitions[n];
  },
  getWinningMoves: function() {
    var wm = [];
    if(!this.current.kernel) {
      for(let i = 0; i < this.current.transitions.length; i++) {
        if(this.current.transitions[i].kernel) {
          wm.push(i);
        }
      }
    }
    return wm;
  }
};

function State(forest, parent, minindex) {
  this.forest = forest;
  this.minindex = minindex;
  this.parent = parent;
  this.traces = new Array(forest.length);
  this.transitions = new Array(forest.size);
  this.kernel = true;
}

State.prototype = {
  transition: function(index, trans) {
    this.transitions[index] = trans;
  }
};

function Automata(maxSize) {
  this.states = new Array(maxSize + 1); 
  for(let i = 0; i <= maxSize; i++) {
    this.states[i] = new Array(0);
  }
}

Automata.prototype = {
  add: function(states) {
    if(states.length > 0) {
      this.states[states[0].forest.size] =
        this.states[states[0].forest.size].concat(states);
    }
  }
};

function Transition(state, node) {
  this.state = state;
  if(node === undefined) {
    this.nodes = [];
  } else {
    this.nodes = [node];
  }
}

Transition.prototype = {
  resolve: function() {
    var s = this.state;
    this.nodes.forEach(function(i) {
      s = s.transitions[i];
    });
    return s;
  },
  copy: function() {
    var t = new Transition(this.state);
    t.nodes = this.nodes;
    return t;
  }
};

