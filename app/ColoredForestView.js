"use strict";

module.exports = ColoredForestView;

var ColoredTree = require("ColoredTree");
var TreeView = require("TreeView");
var d3 = require("d3");
var l = require("loglevel");

function ColoredForestView(trees, config) {
  TreeView.call(this, new ColoredTree(-1, trees), config);
}

ColoredForestView.prototype = Object.create(TreeView.prototype, {
  compute: {
    value: function() {
      var d3tree = d3.layout.tree().size([
        this.config.width - 4*this.config.radius,
        this.config.height - 4*this.config.radius]);
      d3tree.children(ColoredTree.prototype.childrenAccessor);
      this.nodes = d3tree.nodes(this.tree);
      if(this.nodes[0].children) {
        this.nodes[0].children.forEach(function(n) {
          n.root = true;
        });
      }
      this.nodes.shift();
      this.links = d3tree.links(this.nodes);
    },
    enumerable: true,
    configurable: true,
    writable: true
  }
});
