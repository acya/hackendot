"use strict";

// var l = require("loglevel");

exports.TextInfo = TextInfo;
exports.Button = Button;
exports.Page = Page;

function TextInfo(elt) {
  this.elt = elt;
}

TextInfo.prototype = {
  say: function(str) {
    this.elt.innerHTML = str;
  },
  clear: function() {
    this.elt.innerHTML = "";
  }
};

function Button(elt) {
  this.elt = elt;
}

Button.prototype = {
  setOnclick: function(callback) {
    this.elt.onclick = callback;
  },
  hide: function() {
    this.elt.style.visibility = "hidden";
  },
  show: function() {
    this.elt.style.visibility = "visible";
  }
};

function Page(elt, pages) {
  elt.style.display = "none";
  this.elt = elt;
  this.pages = pages;
}

Page.prototype = {
  show: function() {
    Page.current.elt.style.display = "none";
    Page.current = this;
    this.elt.style.display = "block";
    if(this === Page.main) {
      Page.return.hide();
    } else {
      Page.return.show();
    }
  }
};
