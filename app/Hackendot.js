"use strict";

var d3 = require("d3");
var RootedForestView = require("RootedForestView");
var AutomataView = require("AutomataView");
var DerivationsView = require("DerivationsView");
var Models = require("Models");
var Toolkit = require("Toolkit");
var TextInfo = Toolkit.TextInfo;
var Button = Toolkit.Button;
var Page = Toolkit.Page;

// var l = require("loglevel");

module.exports = (new Hackendot());

function prefix(name, verb, playerPrefix) {
  if(name == "Joueur") {
    return playerPrefix;
  }
  return name + " " + verb;
}

function Hackendot() {

  var sizeGrower = 256;
  var sizeKernel = 48;
  var sizeAutomata = 24;
  var model;
  var pages = {};
  var panel, turninfo, help, error;
  var panelautomata;
  var iabutton, returnbutton;
  var forestView;
  var forestDiv, automataDiv, ulehlaDiv;

  Models.setup(sizeGrower);

  this.main = function() {
    panel = new TextInfo(document.getElementById("panel"));
    panelautomata = new TextInfo(document.getElementById("panelautomata"));
    turninfo = new TextInfo(document.getElementById("current-player"));
    help = new TextInfo(document.getElementById("help"));
    error = new TextInfo(document.getElementById("error"));
    iabutton = new Button(document.getElementById("iaplay"));
    iabutton.hide();
    returnbutton = new Button(document.getElementById("return"));
    returnbutton.hide();
    pages.menu = new Page(document.getElementById("menu"), pages);
    pages.playing = new Page(document.getElementById("playing"), pages);
    pages.kernel = new Page(document.getElementById("kernel"), pages);
    pages.ulehla = new Page(document.getElementById("ulehla"), pages);
    Page.current = pages.menu;
    Page.main = pages.menu;
    Page.return = returnbutton;
    pages.menu.elt.style.display = "block";
    returnbutton.setOnclick(function() {
      pages.menu.show();
    });
    forestDiv = d3.select("#forest");
    automataDiv = d3.select("#automata");
    ulehlaDiv = d3.select("#ulehla");
  };

  this.vsPlayer = function() {
    var s = this.checkSizeGrower();
    if(s.isOk) {
      model = new Models.ModelPlayer();
      pages.playing.show();
      this.createForest(s.size);
      this.startGame();
    }
  };

  this.vsKernel = function() {
    var s = this.checkSizeKernel();
    if(s.isOk) {
      model = new Models.ModelKernel();
      pages.playing.show();
      this.createForest(s.size);
      panel.say("Génération de l'automate...");
      model.createAutomata();
      this.startGame();
    }
  };

  this.vsUlehla = function() {
    var s = this.checkSizeGrower();
    if(s.isOk) {
      model = new Models.ModelUlehla();
      pages.playing.show();
      this.createForest(s.size);
      this.startGame();
    }
  };

  this.showAutomata = function() {
    var s = this.checkSizeKernel();
    if(s.isOk) {
      model = new Models.ModelKernel();
      pages.kernel.show();
      this.createForest(s.size);
      panelautomata.say("Génération de l'automate...");
      model.createAutomata();
      automataDiv.selectAll("*").remove();
      let aw = new AutomataView(model.opponent.ai.automata, {
        div: automataDiv,
        displayNums: true
      });
      panelautomata.say("Représentation de l'automate...");
      aw.render();
      panelautomata.clear();
    }
  };

  this.showDerivations = function() {
    var s = this.checkSizeGrower();
    if(s.isOk) {
      model = new Models.ModelUlehla();
      this.createForest(s.size);
      model.opponent.ai.analyze(model.forest);
      ulehlaDiv.selectAll("*").remove();
      let dw = new DerivationsView(model.opponent.ai, {
        div: ulehlaDiv,
        displayNums: true,
        displayColors: true
      });
      dw.render();
      pages.ulehla.show();
      panel.clear();
    }
  };

  this.startGame = function() {
    panel.say(
      prefix(model.current.name, "est", "Vous êtes") +
      " le premier à jouer");
    this.keepPlaying();
  };

  this.playTurn = function() {
    var self = this; 
    if(model.turn == 1) {
      turninfo.say(
        prefix(model.current.name, "joue", "Vous jouez") +
        " ce premier tour");
    } else {
      turninfo.say(
        "C'est à " +
        prefix(model.current.name, "", "vous") +
        " de jouer");
    }
    forestView = new RootedForestView(model.forest, {div: forestDiv});
    if(model.current.isHuman) {
      help.say("Cliquez sur un noeud pour le supprimer");
      forestView.setOnclickNode(function(d) {
        model.move(d.num);
        self.keepPlaying();
      });
      forestView.render();
    } else {
      forestView.render();
      let wm = model.getWinningMoves();
      if(wm.length == 0) {
        panel.say(model.opponent.name +
                  " n'a pas trouvé de coup gagnant.<br>Il va donc jouer aléatoirement");
        iabutton.setOnclick(function() {
          model.randomMove();
          self.keepPlaying();
        });
      } else {
        panel.say(model.opponent.name + " a trouvé un coup gagnant");
        iabutton.setOnclick(function() {
          model.randomMoveIn(wm);
          self.keepPlaying();
        });
      }
      iabutton.show();
      help.say("Cliquez sur le bouton \"Faire jouer l'IA\"");
    }
  };

  this.keepPlaying = function() {
    forestDiv.selectAll("*").remove();
    panel.clear();
    help.clear();
    turninfo.clear();
    iabutton.hide();
    if(model.isOver) {
      panel.say(
        prefix(model.current.name, " a", "Vous avez") +
        " gagné la partie !");
    } else {
      this.playTurn();
    }
  };

  this.createForest = function(size) {
    panel.say("Génération de la forêt...");
    model.createForest(size);
  };

  this.checkSizeGrower = function() {
    var size = getSize();
    if(size < 0) {
      error.say("La taille doit être un entier strictement positif");
      return {isOk: false};
    }
    if(size > sizeGrower) {
      error.say("La taille d'une forêt à générer ne doit pas excéder " +
                sizeGrower);
      return {isOk: false};
    }
    error.clear();
    return {
      size: size,
      isOk: true
    };
  };

  this.checkSizeKernel = function() {
    var size = getSize();
    if(size < 0) {
      error.say("La taille doit être un entier strictement positif");
      return {isOk: false};
    }
    if(size > sizeKernel) {
      error.say("La taille d'une forêt pour Kernel ne doit pas excéder " +
                sizeKernel);
      return {isOk: false};
    }
    error.clear();
    return {
      size: size,
      isOk: true
    };
  };

  this.checkSizeAutomata = function() {
    var size = getSize();
    if(size < 0) {
      error.say("La taille doit être un entier strictement positif");
      return {isOk: false};
    }
    if(size > sizeAutomata) {
      error.say("La taille d'une forêt pour visualiser l'automate ne doit pas excéder " +
                sizeAutomata);
      return {isOk: false};
    }
    error.clear();
    return {
      size: size,
      isOk: true
    };
  };

  function getSize() {
    return document.getElementById("size").value | 0;
  }

}
