"use strict";

var RootedPlants = require("RootedPlants");
var KernelAI = require("KernelAI");
var UlehlaAI = require("UlehlaAI");
var l = require("loglevel");

exports.setup = setup;
exports.ModelPlayer = ModelPlayer;
exports.ModelKernel = ModelKernel;
exports.ModelUlehla = ModelUlehla;

function setup(sizeGrower) {
  RootedPlants.startGrower(sizeGrower + 1);
}

function Model() {
  var self = this;
  this.isOver = false;
  this.player = {
    name: "Joueur",
    isHuman: true,
    next: () => self.opponent
  };
  this.opponent = {
    next: () => self.player
  };
  this.turn = 1;
}

Model.prototype = {
  createForest: function(size) {
    this.forest = RootedPlants.forest(size);
  },
  randomPlayer: function() {
    return Math.random()*2 < 1 ? this.player : this.opponent;
  },
  move: function(num) {
    this.turn++;
    this.forest = this.forest.deleteNode(num);
    if(!(this.isOver = this.forest.empty())) {
      this.current = this.current.next();
    }
  },
  randomMove: function() {
    var r = Math.floor(Math.random() * this.forest.size);
    this.move(r);
  },
  randomMoveIn: function(nums) {
    var r = Math.floor(Math.random() * nums.length);
    this.move(nums[r]);
  },
  getWinningMoves: function() {
    return [];
  }
};

function ModelPlayer() {
  Model.call(this);
  this.player.name = "Joueur 1";
  this.opponent.name = "Joueur 2";
  this.opponent.isHuman = true;
  this.current = this.player;
}

ModelPlayer.prototype = Object.create(Model.prototype, {});

function ModelKernel() {
  Model.call(this);
  this.opponent.name = "Kernel";
  this.opponent.isHuman = false;
  this.current = this.randomPlayer(); 
}

ModelKernel.prototype = Object.create(Model.prototype, {
  createAutomata: {
    value: function() {
      this.opponent.ai = new KernelAI(this.forest);
    },
    enumerable: true,
    configurable: true,
    writable: true
  },
  move: {
    value: function(num) {
      Model.prototype.move.apply(this, [num]);
      this.opponent.ai.move(num);
    },
    enumerable: true,
    configurable: true,
    writable: true
  },
  getWinningMoves: {
    value: function() {
      return this.opponent.ai.getWinningMoves();
    },
    enumerable: true,
    configurable: true,
    writable: true
  }
});

function ModelUlehla() {
  Model.call(this);
  this.opponent.name = "Ulehla";
  this.opponent.isHuman = false;
  this.opponent.ai = new UlehlaAI();
  this.current = this.randomPlayer(); 
}

ModelUlehla.prototype = Object.create(Model.prototype, {
  move: {
    value: function(num) {
      Model.prototype.move.apply(this, [num]);
    },
    enumerable: true,
    configurable: true,
    writable: true
  },
  getWinningMoves: {
    value: function() {
      this.opponent.ai.analyze(this.forest);
      return this.opponent.ai.getWinningMoves();
    },
    enumerable: true,
    configurable: true,
    writable: true
  }
});
