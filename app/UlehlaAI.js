"use strict";

module.exports = UlehlaAI;

var ColoredTree = require("ColoredTree");
// var l = require("loglevel");

function UlehlaAI() {
}

UlehlaAI.prototype = {
  hasWinningMove: function() {
    for(let i = 0; i <= this.N.length; i++) {
      if(this.N[i] == 1) {
        return true;
      }
    }
  },
  getWinningMoves: function() {
    for(var i = this.N.length - 1; i >= 0; i--) {
      if(this.N[i] == 1) {
        break;
      }
    }
    if(i < 0) {
      return [];
    }
    var f = this.L[i];
    var nums = f.filter(t => t.color.isWhite).map(t => t.num);
    for(let j = i - 1; j >= 0; j--) {
      f = this.L[j];
      nums = nums.map(n => ColoredTree.getNode(f, n))
        .map(function(t) {
          var nodes = t.children.filter(c => c.color.isWhite);
          nodes.unshift(t);
          return nodes;
        })
        .reduce((a, b) => a.concat(b), [])
        .map(t => t.num)
        .filter(n => ColoredTree.countWhiteRoots(
          ColoredTree.deleteNode(f, n)) % 2 == 0);
    }
    return nums;
  },
  analyze: function(forest) {
    this.forest = forest.asColoredTrees();
    this.L = prune(this.forest);
    this.N = parity(this.L);
  }
};

function prune(trees) {
  var L = [trees];
  var l;
  while ((l = ColoredTree.deleteWhiteNodes(L[L.length - 1])).length != 0) {
    L.push(l);
  }
  return L;
}

function parity(L) {
  var N = new Array(L.length);
  for(let i = 0; i < N.length; i++) {
    N[i] = ColoredTree.countWhiteRoots(L[i]) % 2;
  }
  return N;
}

